# wercker-eb-deploy

```
box: java
build:
  steps:
    - script:
        name: setup mvn local repo
        code: |
          mkdir -p $WERCKER_CACHE_DIR/.m2
          ln -sf $WERCKER_CACHE_DIR/.m2 ~/.m2
    - script:
        name: release perform
        code: |
          $WERCKER_SOURCE_DIR/mvnw clean package
          mv $WERCKER_SOURCE_DIR/target/demo.zip $WERCKER_OUTPUT_DIR/
    - script:
        name: generate version-label file from light-tag
        code: |
          echo $(git describe --tags 2>/dev/null) > $WERCKER_OUTPUT_DIR/version-label
deploy-demo:
  # the box already installed aws-cli
  box: cgswong/aws:latest
  steps:
    - fuervo/eb-deploy@1.0.0:
        access-key: $S3_KEY_ID
        secret-key: $S3_KEY_SECRET
        app-name: <enter app name>
        env-name: <enter env name>
        version-label: $VERSION_LABEL
        # default ap-northeast-1
        region: <enter region>
        # ex) s3://elasticbeanstalk-ap-northeast-1-1234567890/path/to/key.zip
        # X_S3_BUCKET=elasticbeanstalk-ap-northeast-1-1234567890
        s3-bucket: $S3_BUCKET
        # X_S3_BUCKET_PATH=path/to
        s3-bucket-path: $S3_BUCKET_PATH
        # X_S3_KEY=key.zip
        s3-key: $S3_KEY
```